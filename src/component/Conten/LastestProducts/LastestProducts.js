
import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';


import new1 from "../../../assets/image/new1.jpg"
import new2 from "../../../assets/image/new2.jpg"
import new3 from "../../../assets/image/new3.jpg"
import new4 from "../../../assets/image/new4.jpeg"
import new5 from "../../../assets/image/new5.jpg"
import new6 from "../../../assets/image/new6.jpg"
import new7 from "../../../assets/image/new7.jpg"
import new8 from "../../../assets/image/new8.jpg"
import { Grid } from '@mui/material';


import AttachMoneyIcon from '@mui/icons-material/AttachMoney';
function LastestProducts() {
    return (
        <Grid  mt={7}>
            <Grid style={{ textAlign: "center", color: "#747171" }}>
                <h2 >Sản phẩm mới</h2>
            </Grid>
            <Grid container mt={5}>
                <Grid item md={3} sm={6}>
                    <Card sx={{ maxWidth: 300 }} mt={5}>
                        <CardMedia
                            component="img"
                            height="300"
                            image={new1}
                            alt="green iguana"

                        />
                        <CardContent style={{ textAlign: "center", height: "70" }}>
                            <Typography gutterBottom variant="h6" component="div" >
                                TATIME
                            </Typography>
                            <Typography variant="p">
                                Time-2542
                            </Typography>
                            <Typography variant="h6">
                           <strike style={{color: "red"}}>500</strike><AttachMoneyIcon />300
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
                {/* sản phẩm 2 */}
                <Grid item md={3} sm={6}>
                    <Card sx={{ maxWidth: 300 }} mt={5}>
                        <CardMedia
                            component="img"
                            height="300"
                            image={new2}
                            alt="green iguana"

                        />
                        <CardContent style={{ textAlign: "center", height: "70" }}>
                            <Typography gutterBottom variant="h6" component="div" >
                                TATIME
                            </Typography>
                            <Typography variant="p">
                                Time-2543
                            </Typography>
                            <Typography variant="h6">
                           <strike style={{color: "red"}}>500</strike><AttachMoneyIcon />350
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
                {/* sản phẩm 3 */}
                <Grid item md={3}  sm={6}>
                    <Card sx={{ maxWidth: 300 }} mt={5}>
                        <CardMedia
                            component="img"
                            height="300"
                            image={new3}
                            alt="green iguana"

                        />
                        <CardContent style={{ textAlign: "center", height: "70" }}>
                            <Typography gutterBottom variant="h6" component="div" >
                                TATIME
                            </Typography>
                            <Typography variant="p">
                                Time-2541
                            </Typography>
                            <Typography variant="h6">
                           <strike style={{color: "red"}}>450</strike><AttachMoneyIcon />300
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
                {/* san phẩm 4 */}
                 <Grid item md={3} sm={6}>
                    <Card sx={{ maxWidth: 300 }} mt={5}>
                        <CardMedia
                            component="img"
                            height="300"
                            image={new4}
                            alt="green iguana"

                        />
                        <CardContent style={{ textAlign: "center", height: "70" }}>
                            <Typography gutterBottom variant="h6" component="div" >
                                TATIME
                            </Typography>
                            <Typography variant="p">
                                Time-2544
                            </Typography>
                            <Typography variant="h6">
                           <strike style={{color: "red"}}>550</strike><AttachMoneyIcon />400
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>

            {/* hàng 2 */}
            <Grid container mt={5}>
                <Grid item md={3}  sm={6}>
                    <Card sx={{ maxWidth: 300 }} mt={5}>
                        <CardMedia
                            component="img"
                            height="300"
                            image={new5}
                            alt="green iguana"

                        />
                        <CardContent style={{ textAlign: "center", height: "70" }}>
                            <Typography gutterBottom variant="h6" component="div" >
                                TATIME
                            </Typography>
                            <Typography variant="p">
                                Time-2545
                            </Typography>
                            <Typography variant="h6">
                           <strike style={{color: "red"}}>600</strike><AttachMoneyIcon />500
                            </Typography>
                            
                        </CardContent>
                    </Card>
                </Grid>
                {/* sản phẩm 2 */}
                <Grid item  md={3} sm={6}>
                    <Card sx={{ maxWidth: 300 }} mt={5}>
                        <CardMedia
                            component="img"
                            height="300"
                            image={new6}
                            alt="green iguana"

                        />
                        <CardContent style={{ textAlign: "center", height: "70" }}>
                            <Typography gutterBottom variant="h6" component="div" >
                                TATIME
                            </Typography>
                            <Typography variant="p">
                                Time-2546
                            </Typography>
                            <Typography variant="h6">
                           <strike style={{color: "red"}}>700</strike><AttachMoneyIcon />550
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
                {/* sản phẩm 3 */}
                <Grid item   md={3} sm={6}>
                    <Card sx={{ maxWidth: 300}} mt={5}>
                        <CardMedia
                            component="img"
                            height="300"
                            image={new7}
                            alt="green iguana"

                        />
                        <CardContent style={{ textAlign: "center", height: "70" }}>
                            <Typography gutterBottom variant="h6" component="div" >
                                TATIME
                            </Typography>
                            <Typography variant="p">
                                Time-2541
                            </Typography>
                            <Typography variant="h6" >
                           <strike style={{color: "red"}}>800</strike><AttachMoneyIcon />700
                            </Typography>
                        </CardContent>
                       
                    </Card>
                </Grid>

                <Grid item   md={3} sm={6}>
                    <Card sx={{ maxWidth: 300 }} mt={5}>
                        <CardMedia
                            component="img"
                            height="300"
                            image={new8}
                            alt="green iguana"

                        />
                        <CardContent style={{ textAlign: "center", height: "70" }}>
                            <Typography gutterBottom variant="h6" component="div" >
                                TATIME
                            </Typography>
                            <Typography variant="p">
                                Time-2541
                            </Typography>
                            <Typography variant="h6">
                           <strike style={{color: "red"}}>500</strike><AttachMoneyIcon />300
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
               
            </Grid>
            <Grid  mt={5} textAlign = "center">
                <Button variant='contained'>
                    VIEW ALL
                </Button>
            </Grid>
        </Grid>
    );
}

export default LastestProducts;