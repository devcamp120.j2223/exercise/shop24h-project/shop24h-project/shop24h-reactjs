import { useEffect, useState } from "react"
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Row, Col, Button } from "reactstrap";
import OrderDetail from "./OrderDetail";
import { Card, CardActions, CardContent, Stack, Rating, Typography, FormGroup, FormControlLabel, Checkbox, Paper, InputBase, Pagination, Grid } from '@mui/material';
import LinearProgress from '@mui/material/LinearProgress';
function ProductsListShop() {

    const [clockList, setCloclName] = useState([])
    const [limit, setLimit] = useState(9);
    const [page, setPage] = useState(1);
    const [noPage, setNoPage] = useState(0);
    const [clockName, setClockName] = useState("")

    const inpNameChangHander = (event) => { setClockName(event.target.value); console.log(clockName)}
    //Pagination
    const changePageHandler = (event, value) => {
        setPage(value)
    }

    const fetchAPI = async (url) => {
        const response = await fetch(url);
        const data = await response.json();
        return data;
    }
   

    useEffect((data) => {
        fetchAPI("http://localhost:8000/product")
            .then((data) => {
                setNoPage(Math.ceil(data.data.length / limit));

                setCloclName(data.data.slice((page - 1) * limit, page * limit));
                console.log(data);
            })
            .catch((error) => {
                console.error(error.message);
            })
    }, [limit, page]);




   
    // const [clockList, setClockList] = useState([])
    // useEffect((data) => {
    //     fetchAPI("http://localhost:8000/product/")
    //         .then((data) => {
    //             console.log(data);
    //             setClockList(data.data)
    //         })
    //         .catch((error) => {
    //             console.error(error.message);
    //         })
    // }, []);


    return (
        <>
            <Container className="mt-3">
                <Row>
                    <Col className="col-3" >

                        <Col className="col-4 p-5" >
                            <Card sx={{ minWidth: 275, color: "#ff3bcc" }}>
                                <CardContent>
                                    <Typography sx={{ fontSize: 20 }} gutterBottom  >
                                        Tìm Sản phẩm
                                    </Typography>
                                    <Typography sx={{ fontSize: 15, color: "black" }} gutterBottom mt={2}  >
                                        Tên Đồng hồ
                                    </Typography>
                                    <Typography variant="h5" component="div">
                                        <Paper
                                            component="form"
                                            sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 230 }}
                                        >
                                            <InputBase
                                                placeholder="Tên" onChange={inpNameChangHander}
                                            />
                                        </Paper>
                                    </Typography>
                                    <Typography sx={{ fontSize: 15, color: "black" }} gutterBottom mt={2} >
                                        Giá sản phẩm
                                    </Typography>
                                    <Typography variant="h5" component="div">
                                        <Paper
                                            component="form"
                                            sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 230 }}
                                        >
                                            <InputBase
                                                placeholder="Min-Max"
                                            />
                                        </Paper>
                                    </Typography>
                                    <Typography mt={3}>
                                        <Button style={{ background: "#03a9f4" }} >Tìm sản phẩm</Button>
                                    
                                    </Typography> <Typography mt={3}> <LinearProgress/></Typography>
                                   
                                    <Typography sx={{ fontSize: 20 }} color="text.black" gutterBottom mt={3}>
                                        Thể Loại
                                    </Typography>
                                    <Typography variant="h5" component="div">
                                        <FormGroup>
                                            <FormControlLabel control={<Checkbox defaultChecked />} label="đồng hồ cơ" />
                                            <FormControlLabel control={<Checkbox defaultChecked />} label="đồng hồ điện tử" />
                                            <FormControlLabel control={<Checkbox defaultChecked />} label="SmartWatch" />
                                        </FormGroup>
                                    </Typography>
                                    <Typography mt={3}> <LinearProgress/></Typography>

                                    <Typography sx={{ fontSize: 20 }} color="text.black" gutterBottom mt={3}>
                                        Suất sứ
                                    </Typography>
                                    <Typography variant="h5" component="div">
                                        <FormGroup>
                                            <FormControlLabel control={<Checkbox defaultChecked />} label="Thủy sĩ" />
                                            <FormControlLabel control={<Checkbox defaultChecked />} label="Nhật Bản" />
                                            <FormControlLabel control={<Checkbox defaultChecked />} label="Trung Quốc" />
                                            <FormControlLabel control={<Checkbox defaultChecked />} label="Việt Nam" />
                                           
                                        </FormGroup>
                                    </Typography>
                                    <Typography mt={3}> <LinearProgress/></Typography> 
                                    <Typography sx={{ fontSize: 20 }} color="text.black" gutterBottom mt={3}>
                                        Rating
                                    </Typography>
                                    <Typography variant="h5" component="div">
                                        <Stack spacing={1}>
                                            <Grid container>
                                                <FormControlLabel control={<Checkbox defaultChecked />} />
                                                <Rating name="half-rating" defaultValue={0} readOnly sx={{ alignItems: "center" }} />
                                            </Grid>
                                            <Grid container>
                                                <FormControlLabel control={<Checkbox defaultChecked />} />
                                                <Rating name="half-rating" defaultValue={1} readOnly sx={{ alignItems: "center" }} />
                                            </Grid>
                                            <Grid container>
                                                <FormControlLabel control={<Checkbox defaultChecked />} />
                                                <Rating name="half-rating" defaultValue={2} readOnly sx={{ alignItems: "center" }} />
                                            </Grid>
                                            <Grid container>
                                                <FormControlLabel control={<Checkbox defaultChecked />} />
                                                <Rating name="half-rating" defaultValue={3} readOnly sx={{ alignItems: "center" }} />
                                            </Grid>
                                            <Grid container>
                                                <FormControlLabel control={<Checkbox defaultChecked />} />
                                                <Rating name="half-rating" defaultValue={4} readOnly sx={{ alignItems: "center" }} />
                                            </Grid>
                                            <Grid container>
                                                <FormControlLabel control={<Checkbox defaultChecked />} />
                                                <Rating name="half-rating" defaultValue={5} readOnly sx={{ alignItems: "center" }} />
                                            </Grid>
                                        </Stack>
                                    </Typography>


                                </CardContent>
                            </Card>
                        </Col>
                    </Col>
                    <Col className="col-9">
                        <Row >
                            {clockList.map((clock, index) => {
                                return (
                                    <OrderDetail key={index} nameProp={clock.name} priceProp={clock.promotionPrice} img={clock.imageUrl} buyProp={clock.buyPrice} id={clock._id}/>
                                )
                            })}
                        </Row>
                    </Col >
                </Row>
                <Grid container justifyContent="end">
                    <Grid item>
                        <Pagination color="secondary"
                            count={noPage} defaultPage={page} onChange={changePageHandler}>
                        </Pagination>
                    </Grid>
                </Grid>
            </Container>
        </>

    )
}
export default ProductsListShop