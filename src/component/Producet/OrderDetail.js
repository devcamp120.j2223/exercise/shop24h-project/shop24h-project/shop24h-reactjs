
import { Col, Row } from "reactstrap";
import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import {Button, Typography} from '@mui/material';
import { useEffect, useState } from "react"

const OrderDetail = ({ nameProp, priceProp, img, buyProp,id }) => {
   
    return (
      
            <Col className="col-4 p-5">
                <Card sx={{ maxWidth: 305, textAlign: "center" }}>
                    <a href = {"/product/" + id} >
                    <CardMedia
                        component="img"
                        height="200"
                        image={img}
                        alt="green iguana"
                    />
                    </a>
                    
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                            <b>
                                {nameProp}
                            </b>
                        </Typography>
                      <Row>
                      <Typography variant="h5" color="text.secondary">
                             <strike style={{ color: "red" }}> {buyProp} </strike> &nbsp; {priceProp}
                        </Typography>
                      </Row>
                    </CardContent>
                </Card>
                

            </Col>
      
    )
}

export default OrderDetail;