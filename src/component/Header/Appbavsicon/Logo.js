
import ScheduleIcon from '@mui/icons-material/Schedule';
import { Grid, Typography } from "@mui/material"
function LogoShop() {
    return (
        <Typography variant="h5" color="red">
            <Grid>
                <ScheduleIcon sx={{width: "100" , height: "100"}} /> &nbsp; 
                 A.T Watch
            </Grid>
        </Typography>
    )
}
export default LogoShop